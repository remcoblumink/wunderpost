#!/usr/bin/python
import urllib, json
import sys
import datetime
import time
import paho.mqtt.client as mqtt #import the client1

from config import *

def signal_handler(signal, frame):
    sys.exit(0)

def set_exit_handler(func):
    signal.signal(signal.SIGTERM, func)
def on_exit(sig, func=None):
    print "exit handler triggered"
    sys.exit(1)


format = "%H:%M:%S"
today = datetime.datetime.today()
s = today.strftime(format)
d = datetime.datetime.strptime(s, format)
t = d.strftime(format)
tijd = urllib.quote(t)
today = datetime.date.today()
print tijd
print("Weather Underground data uploader for station "+ STATIONID)
############
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
############
def on_message(client, userdata, message):
    
    #Store JSON message in variable 
    data = json.loads(message.payload)
    #Decode JSON into sepaate variables for usage in constructing URL
    temp = data["temperature_C"]
    humidity = str(data["humidity"])
    avgWindSpeed = str(data["speed"])
    rain = str(data["rain"])
    #batteryLow = str(data["batteryLow"])
    windDirection = str(data["direction_deg"])
    gustSpeed = str(data["gust"])
    tempf = str(9.0/5.0 * temp + 32)
    #timejson = data[time]

    url = "http://weatherstation.wunderground.com/weatherstation/updateweatherstation.php?ID=" + STATIONID + "&PASSWORD=" + PASSWORD + "&dateutc=" + str(today) + "+" + str(tijd) + "&winddir=" + windDirection + "&windspeedmph=" + avgWindSpeed + "&windgustmph=" + gustSpeed + "&tempf=" + tempf + "&rainin=" + rain + "&humidity=" + humidity + "&action=updateraw"
    print data['time']
    #print url
    response = urllib.urlopen(url)
    print response.read()
########################################
########################################
########################################
########################################
########################################
#print("creating new instance")
client = mqtt.Client("P1") #create new instance
client.on_message=on_message #attach function to callback
#print("connecting to broker")
client.connect(broker_address) #connect to broker
client.loop_start() #start the loop
#print("Subscribing to topic","RTL_433/Raw")
client.subscribe(broker_topic)
time.sleep(15) # wait
client.loop_forever() #stop the loop
