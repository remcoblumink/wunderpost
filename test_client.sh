#!/bin/bash

### A simple script to test MQTT messages

# Author: Remco Blumink <remcoblumink@gmail.com>
# Version 1.0
mosquitto_sub  -h localhost -t RTL_433/Raw -d

